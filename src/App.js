import React from 'react';
import {useSelector,useDispatch} from 'react-redux';
import {incNumber, decNumber} from './actions/index';

function App() {

  const myState = useSelector((state) => state.IncDec)

  const dispatch = useDispatch();

  return (

    <>
    <h1>Counter control using redux</h1>
    <h3>Counter value is {myState}</h3>
    <button onClick={()=> dispatch(incNumber())}> + </button><br/><br/>
    <button onClick={()=> dispatch(decNumber())}> - </button>
    </>
  );
}

export default App;
