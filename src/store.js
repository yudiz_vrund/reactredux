import rootReducer from './reducers/index';

import { applyMiddleware, createStore } from "redux";
import {createLogger} from 'redux-logger';

const logger = createLogger();


const store = createStore(rootReducer,applyMiddleware(logger));





export default store;